// soal 1
var nilai;

// jawaban soal 1
console.log('Jawaban soal 1 : \n');
if (nilai >= 85) {
    hasil1 = 'A'
} else if (nilai >= 75 && nilai < 85) {
    hasil1 = 'B'
} else if (nilai >= 65 && nilai < 75) {
    hasil1 = 'C'
} else if (nilai >= 55 && nilai < 65) {
    hasil1 = 'D'
} else {
    hasil1 = 'E'
}
console.log('Indeks : ' + hasil1);
//=====================================================================================================

// soal 2
var tanggal = 12;
var bulan = 8;
var tahun = 2000;

// jawaban soal 2
console.log('\nJawaban soal 2 : \n');
var bulanString = '';
switch (bulan) {
    case 1: bulanString = 'Januari'; break;
    case 2: bulanString = 'Februari'; break;
    case 3: bulanString = 'Maret'; break;
    case 4: bulanString = 'April'; break;
    case 5: bulanString = 'Mei'; break;
    case 6: bulanString = 'Juni'; break;
    case 7: bulanString = 'Juli'; break;
    case 8: bulanString = 'Agustus'; break;
    case 9: bulanString = 'September'; break;
    case 10: bulanString = 'Oktober'; break;
    case 11: bulanString = 'November'; break;
    case 12: bulanString = 'Desember'; break;
    default: bulanString = bulan; break;
}

console.log(tanggal + ' ' + bulanString + ' ' + tahun);
//=====================================================================================================

// soal 3
var n = 7;

// jawaban soal 3
console.log('\nJawaban soal 3 : \n');
var hasil3 = '';
for (let i = 0; i < n; i++) {
    for (let j = 0; j <= i; j++) {
        hasil3 += '#';
    }
    console.log(hasil3);
    hasil3 = '';
}
//=====================================================================================================

// soal 4
var m = 10;

// jawaban soal 4
console.log('\nJawaban soal 4 : \n');
var hasil4 = '';
var hasil4text = ''
for (let a = 1; a <= m; a++) {
    if (a % 3 === 1) hasil4text = 'Programming';
    if (a % 3 === 2) hasil4text = 'Javascript';
    if (a % 3 === 0) {
        hasil4text = 'VueJS';
        for (let b = 0; b < a; b++) {
            hasil4 += '=';
        }
    };
    console.log(a + ' - I love ' + hasil4text);
    a % 3 === 0 ? console.log(hasil4) : false;
    hasil4 = '';
}
//=====================================================================================================