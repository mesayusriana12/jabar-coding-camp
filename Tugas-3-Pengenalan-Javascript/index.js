// soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
// jawaban soal 1
var hasil1 = pertama.substring(0, 5) + ' ' + pertama.substring(11, 18) + ' ' + kedua.substring(0, 8) + '' + kedua.substring(8, 18).toUpperCase();

console.log(hasil1);
//=====================================================================================================

// soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
// jawaban soal 2

var hasil2a = parseInt(kataPertama) + (parseInt(kataKedua) * parseInt(kataKetiga)) + parseInt(kataKeempat);
// 10 + (2 * 4) + 6

var hasil2b = (parseInt(kataPertama) - parseInt(kataKedua) - parseInt(kataKetiga)) * parseInt(kataKeempat);
// (10 - 2 - 4) * 6

var hasil2c = (parseInt(kataPertama) + parseInt(kataKeempat) - parseInt(kataKetiga)) * parseInt(kataKedua);
// (10 + 6 - 4) * 2

var hasil2d = ((parseInt(kataPertama) + parseInt(kataKedua)) * (parseInt(kataKeempat - parseInt(kataKetiga))));
//(10 + 2) * (6 - 4)

console.log(hasil2a, hasil2b, hasil2c, hasil2d)

//=====================================================================================================

// soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

// jawaban soal 3
console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);