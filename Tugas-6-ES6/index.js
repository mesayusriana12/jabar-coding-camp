// soal 1
const luasDanKelilingPersegiPanjang = (p, l) => {
    console.log(`Luas Persegi Panjang : ${p * l}`);
    console.log(`Keliling Persegi Panjang : ${2 * (p + l)}`);
}

// jawaban soal 1
console.log('Jawaban soal 1 : \n');
luasDanKelilingPersegiPanjang(10, 20);
luasDanKelilingPersegiPanjang(4, 3);

//=====================================================================================================

// soal 2
const yourFullname = (firstName, lastName) => {
    return {
        fullName : () => {
            console.log(`${firstName} ${lastName}`);
        }
    }
}

// jawaban soal 2
console.log('===================================\nJawaban soal 2 : \n');
// Driver code
yourFullname("William", "Imoh").fullName() 

//=====================================================================================================

// soal 3
const {firstName, lastName, address, hobby} = bio = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Rancamanyar",
    hobby: "playing football",
}

// jawaban soal 3
console.log('===================================\nJawaban soal 3 : \n');
// Driver code
console.log(firstName, lastName, address, hobby)

//=====================================================================================================

// soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]

// jawaban soal 4
console.log('===================================\nJawaban soal 4 : \n');

//Driver Code
console.log(combined)

//=====================================================================================================

// soal 5
const planet = "earth" 
const view = "glass" 
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`

// jawaban soal 5
console.log('===================================\nJawaban soal 5 : \n');

console.log(before)