// soal 1

/* 
    Tulis kode function di sini
*/
function next_date(tanggal, bulan, tahun)
{
    var kabisat = tahun % 4 === 0;
    var dayInMonth = [31, (kabisat ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    
    var realMonth = tanggal + 1 > dayInMonth[bulan - 1] ? bulan + 1 : bulan;
    var realYear = realMonth > 12 ? tahun + 1 : tahun;
    
    realMonth = realMonth > 12 ? 1 : realMonth;

    switch(realMonth) {
        case 1: stringMonth = 'Januari'; break;
        case 2: stringMonth = 'Februari'; break;
        case 3: stringMonth = 'Maret'; break;
        case 4: stringMonth = 'April'; break;
        case 5: stringMonth = 'Mei'; break;
        case 6: stringMonth = 'Juni'; break;
        case 7: stringMonth = 'Juli'; break;
        case 8: stringMonth = 'Agustus'; break;
        case 9: stringMonth = 'September'; break;
        case 10: stringMonth = 'Oktober'; break;
        case 11: stringMonth = 'November'; break;
        case 12: stringMonth = 'Desember'; break;
        default: stringMonth = ''; break;
    }

    tanggal = realMonth !== bulan || realYear !== tahun ? 0 : tanggal;

    return `${tanggal + 1} ${stringMonth} ${realYear}`;
}

// jawaban soal 1
console.log('\nJawaban soal 1 : \n');

var tanggal1 = 28
var bulan1 = 2
var tahun1 = 2020

var tanggal2 = 29
var bulan2 = 2
var tahun2 = 2021

var tanggal3 = 31
var bulan3 = 12
var tahun3 = 2020

console.log(next_date(tanggal1, bulan1, tahun1))
console.log(next_date(tanggal2, bulan2, tahun2))
console.log(next_date(tanggal3, bulan3, tahun3))


//=====================================================================================================

// soal 2
/* 
    Tulis kode function di sini
*/
function jumlah_kata(str)
{
    return str.split(' ').filter(function (char) { return char !== '' }).length;
}

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = " Saya Iqbal"
var kalimat_3 = " Saya Muhammad Iqbal Mubarok "

// jawaban soal 2
console.log('\nJawaban soal 2 : \n');

console.log(jumlah_kata(kalimat_1))
console.log(jumlah_kata(kalimat_2))
console.log(jumlah_kata(kalimat_3))

//=====================================================================================================