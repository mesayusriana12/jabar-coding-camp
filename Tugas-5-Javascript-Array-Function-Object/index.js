// soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

// jawaban soal 1
console.log('Jawaban soal 1 : \n');

daftarHewan.sort().forEach(element => { 
    console.log(element);
});

//=====================================================================================================

// soal 2
var data = { name: "John", age: 30, address: "Jalan Pelesiran", hobby: "Gaming" };
var anotherData = { name: "Esa", age: 21, address: "Bandung", hobby: "Coding" };
/* 
    Tulis kode function di sini
*/
function introduce(bio)
{
    return `Nama saya ${bio.name}, umur saya ${bio.age} tahun, alamat saya di ${bio.address}, dan saya punya hobby yaitu ${bio.hobby}`;
}

// jawaban soal 2

console.log('\nJawaban soal 2 : \n');

var perkenalan = introduce(data);
var perkenalan2 = introduce(anotherData);

console.log(perkenalan);
console.log(perkenalan2);
//=====================================================================================================

// soal 3

/* 
    Tulis kode function di sini
*/
function hitung_huruf_vokal(str)
{
    var hurufVokal = ['a', 'i', 'u', 'e', 'o'];
    var count = 0;
    str = str.toString().toLowerCase();

    for (let i = 0; i < str.length; i++) {
        if (hurufVokal.includes(str[i])) count++;
    }
    return count;
}

// jawaban soal 3
console.log('\nJawaban soal 3 : \n');

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
var hitung_3 = hitung_huruf_vokal('Esa')

console.log(hitung_1 , hitung_2, hitung_3) // 3 2

//=====================================================================================================

// soal 4
/* 
    Tulis kode function di sini
*/
function hitung(num)
{
    return (num * 2) - 2;
}

// jawaban soal 4
console.log('\nJawaban soal 4 : \n');

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8
//=====================================================================================================